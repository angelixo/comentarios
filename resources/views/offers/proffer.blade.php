@extends('layouts.layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">
                    Es tu decisión...
                </h1>

                <h2 class="text-center">
                    {{$selection->teacher->user->name}} {{$selection->teacher->apellidos}} te ha seleccionado
                </h2>
                <hr>
                @include("offers.partials.fichaOffer")
            </div>
        </div>

        <div class="row">
            <h3 class="text-center">¿Te interesa?</h3>
            <hr>
            <div class="col-lg-12">
                {{ Form::open(['route' => ['offers.answer',$selection->id], 'method' => 'post']) }}
                <div class="col-lg-6 text-center" >
                    <input type="submit" name="answer" value="NO"  style="padding: 35%;" class="btn btn-danger">
                </div>
                <div class="col-lg-6 text-center">
                    <input type="submit" name="answer" value="SI" style="padding: 35%;" class="btn btn-success">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection