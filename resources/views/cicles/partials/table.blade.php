<table id="table-data" class="table table-striped dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="datatable_info">
    <thead>
        <tr>
            <th style="width: 80px;">#</th>
            <th>Nombre</th>
            <th style="width: 80px;">Tipo</th>
            <th style="width: 80px;">Plan</th>
            <th style="width: 80px;">Created</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($formativeCicles as $formativeCicle)
        <tr>
            {!! Form::open(['method' => 'DELETE', 'route' => ['cicle.destroy', $formativeCicle->id],"id"=> "delete-form"]) !!}
            {!! Form::close() !!}
            <td>
                {{$formativeCicle->id}}
            </td>
            <td>
                <a href="{{route("cicle.show",$formativeCicle->id)}}">{{$formativeCicle->name}}</a>
            </td>
            <td>
                <a href="{{route("cicle.show",$formativeCicle->id)}}">{{$formativeCicle->tipo}}</a>
            </td>
            <td>
                <a href="{{route("cicle.show",$formativeCicle->id)}}">{{$formativeCicle->plan}}</a>
            </td>
            <td>
                {{$formativeCicle->created_at->diffForHumans()}}
            </td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog"></i>
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a class="btn btn-link" href="{{route("cicle.edit",$formativeCicle->id)}}"> <i class="fa fa-edit" ></i> Editar</a></li>
                        <li>
                            <a class="btn btn-link" onclick="document.getElementById('delete-form').submit();"><i class="fa fa-trash"></i> Eliminar</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>