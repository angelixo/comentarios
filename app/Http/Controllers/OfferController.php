<?php
/**
 *
 * Controlador de Ofertas
 *
 * Gestiona la información relevante a las ofertas de empleo
 *
 *
 */

namespace App\Http\Controllers;


/** REQUEST */
use Illuminate\Http\Request;
use App\Http\Requests\OfferStoreRequest;
use App\Http\Requests\OfferUpdateRequest;

/** MODELOS */
use App\Enterprise;
use App\Offer;
use App\OfferSelection;
use App\OfferSubscription;
use App\Student;
use App\Family;

/** MAILS */
use App\Mail\OffertAssigned;
use Illuminate\Support\Facades\Mail;

/** OTROS */
use Illuminate\Support\Facades\Auth;


use Faker\Factory as Faker;
use Carbon\Carbon;
use PhpParser\Node\Expr\Cast\Object_;


/**
 * Class OfferController
 *
 * Gestiona ofertas
 * @package App\Http\Controllers
 */

class OfferController extends Controller
{
    /**
     * Muestra el inicio de ofertas
     *
     * @param Illuminate\Http\Request $request trae los datos para la ordenación
     * @var App\Family $families trae las familais
     * @var App\Offer $offers trae las ofertas ordenadas y paginadas
     * @var Faker $faker trae datos falsos para las citas
     * @var object $quote citas falsas dentro del array 'quotes' en config
     *
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $families = get_model_selectable_by_name(Family::all());
        //$offers = Offer::search($request)->PorConfirmar()->orderBy("created_at","desc")->paginate(10);
        $offers = Offer::search($request)->orderBy("created_at","desc")->paginate(10);
        
        $faker = Faker::create("es_ES");
        $quote = $faker->randomElement(config("quotes"));

        $dataMaxSalary = Offer::max("salary"); 
        $dataFromSalary = (isset($_GET["salario"])) ? explode(";", $_GET["salario"])[0] : 0; 
        $dataToSalary = (isset($_GET["salario"])) ? explode(";", $_GET["salario"])[1] : $dataMaxSalary; 
 
        return view('offers.index',compact('offers',"quote","families","request", "dataMaxSalary", "dataToSalary", "dataFromSalary"));  
    }

    /**
     * Muestra el formulario para crear una nueva oferta
     *
     * @var Family $families trae el modelo para familais
     *
     * @var Enterprise $enterprises trae las empresas
     * @var Object $opts contiene los datos secundarios
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $families = get_model_selectable_by_name(Family::all());

        if (auth()->user()->rol == "is_admin"){
            $enterprises = get_model_selectable_by_name(Enterprise::all());
            $opts = compact("families","enterprises");
        }else{
            $opts = compact("families");
        }

        return view('offers.create', $opts);
    }

    /**
     * Almacena una nueva oferta
     *
     * Valida los datos con el request apropiado y almacena.
     *
     *
     * @param  \Illuminate\Http\Request $request trae los datos del formulario
     *
     * @var Offer $offer contenedor de la oferta
     *
     * @return \Illuminate\Http\Response
     */
    public function store(OfferStoreRequest $request)
    {
        //Crear OFERTA
        $offer = new Offer;
        //id del auth

        if (auth()->user()->rol == "is_admin"){
            $offer->enterprise_id = $request->get("enterprise_id");
        }else{
            $offer->enterprise_id = auth()->user()->enterprise->id;
        }


        $offer->requirements = $request->requirements;
        $offer->recommended = $request->recommended;
        $offer->description = $request->description;
        $offer->title = $request->title;
        $offer->work_day = $request->work_day;
        $offer->schedule = $request->schedule;
        $offer->contract = $request->contract;
        $offer->salary = $request->salary;
        $offer->status = "Pend_Validacion";
        $offer->student_number = $request->student_number;
        $offer->start_date = $request->start_date;
        $offer->end_date = $request->end_date;
        $offer->family_id = $request->family_id;

        $respuesta = $offer->save();

        //Crear y guardar los modelos de las relaciones
        return redirect()->route("offers.index");
    }

    /**
     * Muestra una oferta
     *
     * @param  \App\Offer $offer oferta a mostrar
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        return view('offers.show',compact('offer'));
    }

    /**
     * Muestra el formulario para editar una oferta
     *
     * @param  \App\Offer $offer oferta a ser editada
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        $families = get_model_selectable_by_name(Family::all());

        return view('offers.edit',compact('offer', "families"));
    }

    /**
     * Actualiza una oferta en la base de datos
     *
     *
     * @param  \Illuminate\Http\Request $request datos de la oferta
     * @param  \App\Offer $offer oferta a ser modificada
     * @return \Illuminate\Http\Response
     */
    public function update(OfferUpdateRequest $request, Offer $offer)
    {

        $offer->fill($request->only(["requirements", "recommended", "title", "work_day", "schedule", "contract", "start_date", "description", "salary", "student_number", "family_id"]));
        $offer->status = "Pend_Validacion";
        $offer->save();

        return redirect()->route("offers.index");
    }

    /**
     * Elimina una oferta de la bbdd
     *
     * @param  \App\Offer $offer oferta a ser eliminada
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        $offer->delete();

        return redirect()->route("offers.index");
    }

    /**
     * Inactive
     *
     * Mustra las ofertas marcadas como inactivas
     * @return mixed
     */

    public function inactive()
    {
        $offers = Offer::NoActive()->get();
        return view("offers.inactive",compact("offers"));
    }

    /**
     * Activate
     *
     * Muestra los alumnos que no estan en la oferta para que puedan ser incluidos en esta
     * @param Offer $offer oferta
     * @var array $aux contiene los datos de los alumnos seleccionados ya en la oferta
     * @var Student $student contiene los alumnos que No estan en la oferta
     * @var Object $selected muestra los alumnos seleccionados dentro de la oferta
     * @return mixed
     */

    public function activate(Offer $offer)
    {
        Carbon::setLocale('es');
        $aux = [];
        //Mete en un array todos los ids de los estudiantes que ya estan en la oferta
        foreach ($offer->selections as $selected) {
            $aux[] = $selected->student->id;
        }

        //Selecciona todos los alumnos que no estén en aux (en la oferta) [40,120,10]
        $students = Student::Active()->whereNotIn('id',$aux)->orderBy("apellidos")->get();
        $selected = $offer->selections;
        return view("offers.validate", compact("offer", "students","selected"));
    }

    /**
     *
     * Asignador de ofertas
     *
     * Asigna la oferta a uno, o varios alumnos, enviadno un email a éste para darle cuenta.
     *
     * @param Request $request Datos de los alumnos a los que se les asignará la oferta
     * @param Offer $offer Oferta a ser asignada
     *
     *
     * @return mixed
     */

    public function assign(Request $request, Offer $offer)
    {
        $teacher = auth()->user()->teacher;
        if (count($request->get("students")) > 0) {
            foreach ($request->get("students") as $studentRequested) {
                //Encuentra el Alumno
                $student = Student::findOrFail($studentRequested);

                //Comprueba si ese alumno ya ha sido asignado a esa oferta
                $selections = OfferSelection::where("offer_id",$offer->id)->where("student_id",$student->id)->get();
                if ($selections->isEmpty()){
                    $selections_opt = ["student_id" => $student->id, "offer_id" => $offer->id];
                    $teacher->selections()->create($selections_opt)->save();
                    //ENVIO DE CORREOS AQUÍ!
                    Mail::to($student->user->email)->send(new OffertAssigned($student,$offer));
                }
            }

        }


        $offer->status = "Pend_Confirmacion";
        $offer->save();
        //ENVIO DE CORREOS AQUÍ!

        return redirect()->route("offers.activate",$offer->id);
    }

    /**
     * Eliminar seleccionados
     *
     *Elimina los alumnos seleccionados de una oferta
     *
     * @param OfferSelection $selected alumnos seleccionados
     * @return mixed
     *
     */

    public function selectedDelete(OfferSelection $selected)
    {
        $id = $selected->offer->id;
        $selected->delete();

        return redirect()->route("offers.activate",$id);
    }

    /**
     * Suscribir
     * Suscribe al usuario logeado  una oferta
     * @param Offer $offer oferta
     * @return mixed
     */

    public function subscribe(Offer $offer)
    {
        if (!$offer->subscribed_by_auth){
            auth()->user()->student->subscriptions()->create(["offer_id"=>$offer->id])->save();
        }

        return redirect()->back();
    }

    /**
     * Desuscribir
     * Elimina al usuario logeado de la suscripcion a una oferta
     * @param Offer $offer oferta
     * @return mixed
     */

    public function unsubscribe(Offer $offer)
    {
        if ($offer->subscribed_by_auth){
            $sub = OfferSubscription::where("offer_id",$offer->id)->where("student_id",auth()->user()->student->id)->first();
            $sub->delete();
        }

        return redirect()->back();
    }


    public function proffer(OfferSelection $selection)
    {

        $offer = $selection->offer;
        return view("offers.proffer",compact("offer","selection"));
    }

    /**
     * Confirmación de asignación
     * @param Request $request
     * @param OfferSelection $selection
     * @return mixed
     */

    public function answer(Request $request,OfferSelection $selection)
    {
        $answer = ($request->get("answer") == "SI")?"1":"0";
        $selection->fill(["answer" => $answer])->save();

        if ($answer == "1")
            return redirect()->route("offers.show",$selection->offer->id);
        else
            return redirect()->route("home");

    }
}
