<?php
/**
 *
 * Controlador Familia profesional
 *
 * Gestionala información relativa a las familias profeionales
 */

namespace App\Http\Controllers;

use App\Family;
use App\Http\Requests\ProfessionalFamilyStoreRequest;
use App\ProfessionalFamily;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\ProfessionalFamilyUpdateRequest;

/**
 * Class ProfessionalFamilyController
 *
 * Gestiona la información relativa a las familias profesionales
 * @package App\Http\Controllers
 */

class ProfessionalFamilyController extends Controller
{
    /**
     * Muestra el inicio
     * @param Illuminate\Http\Request $request trae los datos para la ordenación
     * @app App\ProfessionalFamily $profesionalfamilys familias profesionales, paginadas
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $profesionalfamilys = ProfessionalFamily::Name($request)->paginate();

        return view("family.index",compact("profesionalfamilys","request"));
    }

    /**
     * Muesta el formulario de creación
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('family.create');
    }

    /**
     * Almacena una nueva familia profesional en la bbdd
     *
     * @param  \Illuminate\Http\Request  $request datos del formulario
     * @var Family $family familia creada
     * @return \Illuminate\Http\Response
     */
    public function store(ProfessionalFamilyStoreRequest $request)
    {
        $family = new ProfessionalFamily(["nombre" => $request->get("nombre")]);
        $family->save();
        Session::flash('message', 'Familia profesional agregada correctamente');
        return redirect()->route("family.index");
    }

    /**
     * Muestra los datos de una familia en concreto
     *
     * @param  \App\ProfessionalFamily  $family familia concreta
     * @return \Illuminate\Http\Response
     */
    public function show(ProfessionalFamily $family)
    {
        //dd($family);
        return view("family.show",compact("family"));
    }

    /**
     * Muestra el formulario para editar una familia profeional
     *
     *
     * @param  \App\ProfessionalFamily  $family familia a editar
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfessionalFamily $family)
    {
        return view("family.edit",compact("family"));
    }

    /**
     * Actualiza una familia en la base de datos.
     *
     * @param  \Illuminate\Http\Request  $request trae los datos del formulario
     * @param  \App\ProfessionalFamily  $family familia a ser editada
     * @return \Illuminate\Http\Response
     */
    public function update(ProfessionalFamilyUpdateRequest $request, ProfessionalFamily $family)
    {
        //dd($family);
        $family->fill(["nombre" => $request->get("nombre")]);
        $family->save();
        Session::flash('message', 'Familia profesional editada correctamente');

        return redirect()->route("family.index");
    }

    /**
     * Elimina una familia de la bbdd.
     *
     * @param  \App\ProfessionalFamily  $family familia a ser eliminada
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfessionalFamily $family)
    {
        $family->delete();

        Session::flash('message', 'La Familia profesional '.$family->nombre.' ha sido eliminada!!! ');

        return redirect()->route("family.index");
    }
}
