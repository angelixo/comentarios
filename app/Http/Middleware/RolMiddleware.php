<?php
/**
 * rolMiddlewhere
 *
 * Verifica que el rol sea el correcto para esa sección de la web
 */
 
namespace App\Http\Middleware; 
 
use Closure; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
 
class RolMiddleware 
{ 
    public function handle($request, Closure $next, $rol) { 
        $rols = explode('|', $rol); //Transforma los roles en arrays separados por '|'
        
        if(Auth::check()){  //Está logueado 
            foreach($rols as $rol){
                if(Auth::user()->rol == $rol) {   //Rol OK 
                    return $next($request); 
                }
            }
                return response()->view("errors.401",[],401);  //Rol no ok
        }else{  //No está logueado 
            return redirect('login'); 
        }
    } 
} 